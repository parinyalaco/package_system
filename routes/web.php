<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\ProductGroupsController;
use App\Http\Controllers\PackageTypesController;
use App\Http\Controllers\StampsController;
use App\Http\Controllers\PackMachinesController;
use App\Http\Controllers\PackagesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PackagingsController;
use App\Http\Controllers\UsersController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/', [PackagingsController::class, 'index']);


Route::resource('customers', CustomersController::class);
Route::resource('product-groups', ProductGroupsController::class);
Route::resource('package-types', PackageTypesController::class);
Route::resource('pack-machines', PackMachinesController::class);
Route::resource('stamps', StampsController::class);



Route::get('packages/getpackagelist/', [PackagesController::class, 'getpackagelist']);

Route::resource('packages', PackagesController::class);
Route::resource('products', ProductsController::class);

Route::get('packagings/clone/{id}', [PackagingsController::class, 'clone']);
Route::get('packagings/showfile/{id}', [PackagingsController::class, 'showfile']);
Route::get('packagings/downloadfile/{id}/{key}', [PackagingsController::class, 'downloadfile']);
Route::get('packagings/createwithadd', [PackagingsController::class, 'createwithadd']);
Route::post('packagings/createwithaddAction', [PackagingsController::class, 'createwithaddAction']);
Route::get('packagings/editwithadd/{id}', [PackagingsController::class, 'editwithadd']);
Route::post('packagings/editwithaddAction/{id}', [PackagingsController::class, 'editwithaddAction']);
Route::get('packagings/deletepackage/{packaging_id}/{package_type_id}/{package_id}', [PackagingsController::class, 'deletepackage']);
Route::get('packagings/exportdoc/{id}', [PackagingsController::class, 'exportdoc']);
Route::get('packagings/exportexcel/{id}', [PackagingsController::class, 'exportexcel']);


Route::resource('packagings', PackagingsController::class);

Route::get('users/changerole/{id}', [UsersController::class, 'changerole']);

Route::resource('users', UsersController::class);


