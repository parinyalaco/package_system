<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packagings', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->string('version', 100);
            $table->text('desc')->nullable();
            $table->float('inner_weight_g')->nullable();
            $table->integer('number_per_pack')->nullable();
            $table->float('outer_weight_kg')->nullable();
            $table->string('status', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packagings');
    }
}
