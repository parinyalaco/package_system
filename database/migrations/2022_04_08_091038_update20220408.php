<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220408 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'packages',
            function (Blueprint $table) {
                $table->string('sapnote')->nullable();
                $table->string('note2')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('packages', 'sapnote')) {
            Schema::table('packages', function (Blueprint $table) {
                $table->dropColumn('sapnote');
            });
        }
        if (Schema::hasColumn('packages', 'note2')) {
            Schema::table('packages', function (Blueprint $table) {
                $table->dropColumn('note2');
            });
        }
    }
}
