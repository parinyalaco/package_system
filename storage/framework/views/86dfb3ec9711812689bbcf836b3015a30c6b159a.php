<div class="row">
<div class="col-md-4 <?php echo e($errors->has('product_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('product_id', 'Product', ['class' => 'control-label']); ?>

        <?php if(isset($packaging->product_id)): ?>
            <?php echo Form::select('product_id', $productlist,$packaging->product_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('product_id', $productlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('product_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('start_date') ? 'has-error' : ''); ?>">
    <label for="start_date" class="control-label"><?php echo e('Start Date'); ?></label>
    <input class="form-control" name="start_date" type="date" id="start_date" value="<?php echo e($packaging->start_date ?? \Carbon\Carbon::now()->format('Y-m-d')); ?>" >
    <?php echo $errors->first('start_date', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('end_date') ? 'has-error' : ''); ?>">
    <label for="end_date" class="control-label"><?php echo e('End Date'); ?></label>
    <input class="form-control" name="end_date" type="date" id="end_date" value="<?php echo e($packaging->end_date ?? \Carbon\Carbon::now()->format('Y-m-d')); ?>" >
    <?php echo $errors->first('end_date', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-8 <?php echo e($errors->has('version') ? 'has-error' : ''); ?>">
    <label for="version" class="control-label"><?php echo e('Version name'); ?></label>
    <input class="form-control" name="version" type="text" id="version" required value="<?php echo e($packaging->version ?? ''); ?>" >
    <?php echo $errors->first('version', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
    <label for="status" class="control-label"><?php echo e('status'); ?></label>
    <input class="form-control" name="status" type="text" id="status" required value="<?php echo e($packaging->status ?? 'Active'); ?>" >
    <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-4  <?php echo e($errors->has('inner_weight_g') ? 'has-error' : ''); ?>">
    <label for="inner_weight_g" class="control-label"><?php echo e('น้ำหนัก/ถุง (g.)'); ?></label>
    <input class="form-control calweight" name="inner_weight_g" type="text" id="inner_weight_g" value=<?php if(!empty($packaging->inner_weight_g)): ?>
    "<?php echo e(number_format($packaging->inner_weight_g,2,".",",") ?? ''); ?>"
    <?php else: ?>
        "0"
    <?php endif; ?>    
     >
    <?php echo $errors->first('inner_weight_g', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('number_per_pack') ? 'has-error' : ''); ?>">
    <label for="number_per_pack" class="control-label"><?php echo e('จำนวนถุง/กล่อง'); ?></label>
    <input class="form-control calweight" name="number_per_pack" type="text" id="number_per_pack" value="<?php echo e($packaging->number_per_pack ?? '0'); ?>" >
    <?php echo $errors->first('number_per_pack', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('outer_weight_kg') ? 'has-error' : ''); ?>">
    <label for="outer_weight_kg" class="control-label"><?php echo e('น้ำหนัก/กล่อง (kg.)'); ?></label>
    <input class="form-control" name="outer_weight_kg" type="text" id="outer_weight_kg" 
    value=<?php if(!empty($packaging->outer_weight_kg)): ?> "<?php echo e(number_format($packaging->outer_weight_kg,3,".",",") ?? ''); ?>" <?php else: ?> "0" <?php endif; ?>
    >
    <?php echo $errors->first('outer_weight_kg', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e($packaging->desc ?? ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-12">
    <div class="row">
<?php $__currentLoopData = $packagetypelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $packagetypeObj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(isset($packagearr[$packagetypeObj->id])): ?>
<div class="col-md-4 <?php echo e($errors->has('package_id-'.$packagetypeObj->id) ? 'has-error' : ''); ?>">
        
        
       
        <?php if(isset($packageid[$packagetypeObj->id])): ?>
            <?php echo Form::checkbox('package_id-chk-'.$packagetypeObj->id,$packagetypeObj->id,true,['class'=>'packagedata']);; ?>

            <?php echo Form::label('package_id-'.$packagetypeObj->id, $packagetypeObj->name , ['class' => 'control-label']); ?>

             <div id="dd-<?php echo e($packagetypeObj->id); ?>" style='display: block'>
            <?php echo Form::select('package_id-'.$packagetypeObj->id, $packagearr[$packagetypeObj->id],$packageid[$packagetypeObj->id], ['class' => 'form-control caldate getorderlist getprice','placeholder' => '===Select===']); ?>   
        <?php else: ?>
            <?php echo Form::checkbox('package_id-chk-'.$packagetypeObj->id,$packagetypeObj->id,false,['class'=>'packagedata']);; ?>

            <?php echo Form::label('package_id-'.$packagetypeObj->id, $packagetypeObj->name , ['class' => 'control-label']); ?>

             <div id="dd-<?php echo e($packagetypeObj->id); ?>" style='display: none'>
            <?php echo Form::select('package_id-'.$packagetypeObj->id, $packagearr[$packagetypeObj->id],null, ['class' => 'form-control caldate getorderlist  getprice','placeholder' => '===Select===']); ?>

        <?php endif; ?>
        </div>
        
        <?php echo $errors->first('package_id-'.$packagetypeObj->id, '<p class="help-block">:message</p>'); ?>

</div>      
<?php endif; ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div> </div>
<div class="col-md-3">
     <?php echo Form::label('stamp_id_head','เครื่อง Stamp'  , ['class' => 'control-label']); ?>

</div>
<div class="col-md-9">
    <?php $__currentLoopData = $stamplist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!empty($stampid) && in_array($key,$stampid)): ?>
        <?php echo Form::checkbox('stamp_id[]', $key, true);; ?>

    <?php else: ?>
        <?php echo Form::checkbox('stamp_id[]', $key);; ?>

    <?php endif; ?>
      
        <?php echo Form::label('stamp_id[]', $item , ['class' => 'control-label']); ?>

      
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
</div>
<div class="col-md-3">
     <?php echo Form::label('pack_machine_id_head','เครื่อง Pack'  , ['class' => 'control-label']); ?>

</div>
<div class="col-md-9">
    <?php $__currentLoopData = $packmachinelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <?php if(!empty($packmachineid) && in_array($key,$packmachineid)): ?>
        <?php echo Form::checkbox('pack_machine_id[]', $key, true);; ?>

    <?php else: ?>
        <?php echo Form::checkbox('pack_machine_id[]', $key);; ?>

    <?php endif; ?>
        <?php echo Form::label('pack_machine_id[]', $item , ['class' => 'control-label']); ?>

        
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
</div>
<div class="col-md-12">
    <input class="btn btn-primary form-group" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'Create'); ?>">
</div>
</div>

    <script>$(document).ready(function () {
        $('.calweight').change(function() {
            var innerweight = parseFloat($('#inner_weight_g').val());
            var numperpack= parseInt($('#number_per_pack').val());
            var totalweight = (innerweight*numperpack/1000);
            $('#outer_weight_kg').val(totalweight);
        });

        $(".packagedata").click(function(){
            $( "#dd-"+$(this).val() ).toggle( "slow" );
        });
    });
    </script><?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/packagings/form.blade.php ENDPATH**/ ?>