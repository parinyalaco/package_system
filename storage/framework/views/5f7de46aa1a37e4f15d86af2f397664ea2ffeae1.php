 <?php if (isset($component)) { $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da = $component; } ?>
<?php $component = $__env->getContainer()->make(App\View\Components\AppLayout::class, []); ?>
<?php $component->withName('app-layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
     <?php $__env->slot('header'); ?> 
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Product <?php echo e($product->id); ?>

        </h2>
     <?php $__env->endSlot(); ?>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="<?php echo e(url('/products')); ?>" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="<?php echo e(url('/products/' . $product->id . '/edit')); ?>" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="<?php echo e(url('products' . '/' . $product->id)); ?>" accept-charset="UTF-8" style="display:inline">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td><?php echo e($product->id); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Customer</th><td><?php echo e($product->customer->name); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Product Group</th><td><?php echo e($product->productgroup->name); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Name</th><td><?php echo e($product->name); ?></td>
                                    </tr>
                                    <tr>
                                        <th>SAP</th><td><?php echo e($product->SAP); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Shelf Lift (เดือน)</th><td><?php echo e($product->shelf_life); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td><?php echo e($product->desc); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td><?php echo e($product->status); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                         <h4>Packaging</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Packaging version</th>
                                <th>Product</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $packagings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php echo e($item->version); ?></td>
                                        <td>
                                        <?php echo e($item->product->name ?? ''); ?></td>
                                    <td><a href="<?php echo e(url('/packagings/' . $item->id . '/edit')); ?>" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
</td>
                                </tr>    
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </tbody>
                    </table>
                     <div class="pagination-wrapper"> <?php echo $packagings->render(); ?> </div>
            </div>
            </div>
        </div>
    </div>
 <?php if (isset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da)): ?>
<?php $component = $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da; ?>
<?php unset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 


<?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/products/show.blade.php ENDPATH**/ ?>