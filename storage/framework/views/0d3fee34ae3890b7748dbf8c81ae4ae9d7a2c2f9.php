 <?php if (isset($component)) { $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da = $component; } ?>
<?php $component = $__env->getContainer()->make(App\View\Components\AppLayout::class, []); ?>
<?php $component->withName('app-layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
     <?php $__env->slot('header'); ?> 
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Packagings
        </h2>
     <?php $__env->endSlot(); ?>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <?php if(Auth::user()->grouptype == 'Admin'): ?>                                                    
                            <a href="<?php echo e(url('/packagings/createwithadd')); ?>" class="btn btn-success btn-sm" title="Add New Packaging">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        <?php endif; ?>
                            <div class="input-group">
                                <span class="input-group-append">
                                </span>
                            </div>
                        

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="<?php echo e(url('/packagings')); ?>" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">                        
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Version<br/><input type="text" class="form-control" name="txtversion" placeholder="ค้นหา Verion..." value="<?php echo e(request('txtversion')); ?>"></th>
                                        <th>Product Group<br/><input type="text" class="form-control" name="txtproductgroup" placeholder="ค้นหา กลุ่มสินค้า..." value="<?php echo e(request('txtproductgroup')); ?>"></th>                                                 
                                        <th>Product<br/><input type="text" class="form-control" name="txtproduct" placeholder="ค้นหา สินค้า..." value="<?php echo e(request('txtproduct')); ?>"></th>         
                                        <th>Package<br/><input type="text" class="form-control" name="txtpackage" placeholder="ค้นหา Package..." value="<?php echo e(request('txtpackage')); ?>"></th>                                    
                                        <th>น้ำหนักต่อถุง(g.) <br/>/ จำนวนถุงต่อกล่อง<br/> / น้ำหนักต่อกล่อง(kg.)<br/><input type="text" class="form-control" name="txtweight" placeholder="ค้นหา นน จำนวน..." value="<?php echo e(request('txtweight')); ?>"></th>
                                        <th>Actions<br/>
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                <?php $__currentLoopData = $packagings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->version); ?></td>                                        
                                        <td><?php echo e($item->product->productgroup->name ?? ''); ?></td>
                                        <td><?php echo e($item->product->name); ?></td>
                                        
                                        <td>
                                            <?php $__currentLoopData = $item->package()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php echo e($subitem->packagetype->name); ?> \ <?php echo e($subitem->name); ?> <br/>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                        <td><?php echo e(number_format($item->inner_weight_g,2,".",",")); ?> / <?php echo e($item->number_per_pack); ?> / <?php echo e(number_format($item->outer_weight_kg,3,".",",")); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('/packagings/' . $item->id)); ?>" title="View Packaging"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="<?php echo e(url('/packagings/exportdoc/' . $item->id)); ?>" title="QP07 Packaging"><button class="btn btn-info btn-sm"><i class="fa fa-file-text-o" aria-hidden="true"></i> QP07</button></a>
                                 
                                            <?php if(Auth::user()->grouptype == 'Admin'): ?> 
                                            <a href="<?php echo e(url('/packagings/clone/' . $item->id )); ?>" title="Edit Packaging"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> clone</button></a>

                                            <a href="<?php echo e(url('/packagings/editwithadd/' . $item->id )); ?>" title="Edit Packaging"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="<?php echo e(url('/packagings' . '/' . $item->id)); ?>" accept-charset="UTF-8" style="display:inline">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Packaging" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $packagings->appends([
                                'txtversion' => Request::get('txtversion'),
                                'txtproductgroup' => Request::get('txtproductgroup'),
                                'txtproduct' => Request::get('txtproduct'),
                                'txtpackage' => Request::get('txtpackage'),
                                'txtweight' => Request::get('txtweight')
                                ])->render(); ?> </div>
                        </div>

            </div>
        </div>
    </div>
 <?php if (isset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da)): ?>
<?php $component = $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da; ?>
<?php unset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> <?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/packagings/index.blade.php ENDPATH**/ ?>