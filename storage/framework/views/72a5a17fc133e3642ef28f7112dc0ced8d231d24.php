 <?php if (isset($component)) { $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da = $component; } ?>
<?php $component = $__env->getContainer()->make(App\View\Components\AppLayout::class, []); ?>
<?php $component->withName('app-layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
     <?php $__env->slot('header'); ?> 
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Products
        </h2>
     <?php $__env->endSlot(); ?>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="<?php echo e(url('/products/create')); ?>" class="btn btn-success btn-sm" title="Add New Product">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="<?php echo e(url('/products')); ?>" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer<br/>
                                            <input type="text" class="form-control" name="txtcustomer" placeholder="ค้นลูกค้า..." value="<?php echo e(request('txtcustomer')); ?>">
                                        </th>
                                        <th>
                                            Product Group<br/>
                                            <input type="text" class="form-control" name="txtproductgroup" placeholder="ค้นกลุ่มสินค้า..." value="<?php echo e(request('txtproductgroup')); ?>">
                                        </th>
                                        <th>Product Name<br/>
                                            <input type="text" class="form-control" name="txtproduct" placeholder="ค้นสินค้า..." value="<?php echo e(request('txtproduct')); ?>">
                                        </th>
                                        <th>Used</th>
                                        <th>Actions<br/><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->customer->name ?? ''); ?></td>
                                        <td><?php echo e($item->productgroup->name ?? ''); ?></td>
                                        <td><?php echo e($item->name); ?></td>
                                        <td><?php echo e($item->packagings()->count() ?? '0'); ?></td>
                                        
                                        <td>
                                            <a href="<?php echo e(url('/products/' . $item->id)); ?>" title="View Product"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="<?php echo e(url('/products/' . $item->id . '/edit')); ?>" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            <?php if($item->packagings()->count() == 0): ?>
                                            <form method="POST" action="<?php echo e(url('/products' . '/' . $item->id)); ?>" accept-charset="UTF-8" style="display:inline">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>    
                                            <?php endif; ?>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            
                            <div class="pagination-wrapper"> <?php echo $products->appends(
                                [
                                    'txtcustomer' => Request::get('txtcustomer'),
                                    'txtproductgroup' => Request::get('txtproductgroup'),
                                    'txtproduct' => Request::get('txtproduct')
                                ])->render(); ?> </div>
                        </div>
            </div>
        </div>
    </div>
 <?php if (isset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da)): ?>
<?php $component = $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da; ?>
<?php unset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
<?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/products/index.blade.php ENDPATH**/ ?>