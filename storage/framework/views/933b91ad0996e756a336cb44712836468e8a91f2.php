<div class="row">
<div class="col-md-4 <?php echo e($errors->has('package_type_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('package_type_id', 'Package Type', ['class' => 'control-label']); ?>

        <?php if(isset($package->package_type_id)): ?>
            <?php echo Form::select('package_type_id', $packagetypelist,$package->package_type_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('package_type_id', $packagetypelist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('package_type_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e($package->name ?? ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
    <label for="status" class="control-label"><?php echo e('status'); ?></label>
    <input class="form-control" name="status" type="text" id="status" required value="<?php echo e($package->status ?? 'Active'); ?>" >
    <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e($package->desc ?? ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-6  <?php echo e($errors->has('stamp_format') ? 'has-error' : ''); ?>">
    <label for="stamp_format" class="control-label"><?php echo e('Stamp Format'); ?></label>
    <input class="form-control" name="stamp_format" type="text" id="stamp_format" required value="<?php echo e($package->stamp_format ?? ''); ?>" >
    <?php echo $errors->first('stamp_format', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-6  <?php echo e($errors->has('size') ? 'has-error' : ''); ?>">
    <label for="size" class="control-label"><?php echo e('Size'); ?></label>
    <input class="form-control" name="size" type="text" id="size" required value="<?php echo e($package->size ?? ''); ?>" >
    <?php echo $errors->first('size', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-6  <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
    <label for="image" class="control-label"><?php echo e('Image PATH'); ?></label>
    <input class="form-control" name="image" type="text" id="image" required value="<?php echo e($package->image ?? ''); ?>" >
    <?php echo $errors->first('image', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('relate_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('relate_id', 'Package เก่า (ถ้ามี)', ['class' => 'control-label']); ?>

        <?php if(isset($package->relate_id)): ?>
            <?php echo Form::select('relate_id', $packagelist,$package->relate_id, ['class' => 'form-control caldate getorderlist getprice','placeholder'=>'เลือก']); ?>   
        <?php else: ?>
            <?php echo Form::select('relate_id', $packagelist,null, ['class' => 'form-control caldate getorderlist  getprice','placeholder'=>'เลือก']); ?>

        <?php endif; ?>
        <?php echo $errors->first('relate_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class=" col-md-4">
    <input class="form-group btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'Create'); ?>">
</div>
</div><?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/packages/form.blade.php ENDPATH**/ ?>