 <?php if (isset($component)) { $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da = $component; } ?>
<?php $component = $__env->getContainer()->make(App\View\Components\AppLayout::class, []); ?>
<?php $component->withName('app-layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
     <?php $__env->slot('header'); ?> 
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Packages
        </h2>
     <?php $__env->endSlot(); ?>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="<?php echo e(url('/packages/create')); ?>" class="btn btn-success btn-sm" title="Add New Package">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        
                            
                        

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="<?php echo e(url('/packages')); ?>" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type<br/><input type="text" class="form-control" name="txttype" placeholder="ค้นหาประเภท..." value="<?php echo e(request('txttype')); ?>"></th>
                                        <th>Name<br/><input type="text" class="form-control" name="txtname" placeholder="ค้นหาชื่อ..." value="<?php echo e(request('txtname')); ?>"></th>
                                        <th>Size<br/><input type="text" class="form-control" name="txtsize" placeholder="ค้นหาขนาด..." value="<?php echo e(request('txtsize')); ?>"></th>
                                        <th>Used</th>
                                        <th>Status<br/><?php echo Form::select('txtstatus', array('Active'=>'Active','Inactive'=>'Inactive'),request('txtstatus')); ?></th>                                        
                                        
                                        <th>Actions<br><button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->packagetype->name ?? ''); ?></td>
                                        <td><?php echo e($item->name); ?></td>
                                        <td><?php echo e($item->size); ?></td>
                                        <td><?php echo e($item->packagings()->count() ?? '0'); ?></td>
                                        <td><?php echo e($item->status); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('/packages/' . $item->id)); ?>" title="View Package"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="<?php echo e(url('/packages/' . $item->id . '/edit')); ?>" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            <?php if($item->packagings()->count() == 0): ?>
                                            <form method="POST" action="<?php echo e(url('/packages' . '/' . $item->id)); ?>" accept-charset="UTF-8" style="display:inline">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Package" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>    
                                            <?php endif; ?>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $packages->appends([
                                'txttype' => Request::get('txttype'),
                                'txtname' => Request::get('txtname'),
                                'txtsize' => Request::get('txtsize'),
                                'txtstatus' => Request::get('txtstatus'),
                                ])->render(); ?> </div>
                        </div>
            </div>
        </div>
    </div>
 <?php if (isset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da)): ?>
<?php $component = $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da; ?>
<?php unset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
<?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/packages/index.blade.php ENDPATH**/ ?>