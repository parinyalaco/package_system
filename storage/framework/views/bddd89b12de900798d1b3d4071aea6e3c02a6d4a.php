
<div class="row">
<div class="col-md-4 <?php echo e($errors->has('customer_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('customer_id', 'Customer', ['class' => 'control-label']); ?>

        <?php if(isset($product->customer_id)): ?>
            <?php echo Form::select('customer_id', $customerlist,$product->customer_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('customer_id', $customerlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('customer_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('product_group_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('product_group_id', 'Product Group', ['class' => 'control-label']); ?>

        <?php if(isset($product->customer_id)): ?>
            <?php echo Form::select('product_group_id', $productgrouplist,$product->product_group_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('product_group_id', $productgrouplist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('product_group_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e($product->name ?? ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('shelf_life') ? 'has-error' : ''); ?>">
    <label for="shelf_life" class="control-label"><?php echo e('Shelf life (เดือน)'); ?></label>
    <input class="form-control" name="shelf_life" type="number" id="shelf_life" required value="<?php echo e($product->shelf_life ?? '0'); ?>" >
    <?php echo $errors->first('shelf_life', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
    <label for="status" class="control-label"><?php echo e('status'); ?></label>
    <input class="form-control" name="status" type="text" id="status" required value="<?php echo e($product->status ?? 'Active'); ?>" >
    <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('SAP') ? 'has-error' : ''); ?>">
    <label for="SAP" class="control-label"><?php echo e('SAP'); ?></label>
    <input class="form-control" name="SAP" type="text" id="SAP" required value="<?php echo e($product->SAP ?? ''); ?>" >
    <?php echo $errors->first('SAP', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e($product->desc ?? ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-4 form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'Create'); ?>">
</div>
</div><?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/products/form.blade.php ENDPATH**/ ?>