<?php if (isset($component)) { $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da = $component; } ?>
<?php $component = $__env->getContainer()->make(App\View\Components\AppLayout::class, []); ?>
<?php $component->withName('app-layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
     <?php $__env->slot('header'); ?> 
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Packaging <?php echo e($packaging->id); ?>

        </h2>
     <?php $__env->endSlot(); ?>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                 <a href="<?php echo e(url('/packagings')); ?>" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="<?php echo e(url('/packagings/editwithadd/' . $packaging->id )); ?>" title="Edit Packaging"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="<?php echo e(url('packagings' . '/' . $packaging->id)); ?>" accept-charset="UTF-8" style="display:inline">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Packaging" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td><?php echo e($packaging->id); ?></td>
                                    </tr>
                                    <tr>
                                        <th>สินค้า</th><td><?php echo e($packaging->product->name); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Version</th><td><?php echo e($packaging->version); ?></td>
                                    </tr>
                                    <tr>
                                        <th>รายละเอียด</th><td><?php echo e($packaging->desc); ?></td>
                                    </tr>
                                    <tr>
                                        <th>เริ่มวันที่</th><td><?php echo e($packaging->start_date); ?></td>
                                    </tr>
                                    <tr>
                                        <th>สินสุดวันที่</th><td><?php echo e($packaging->end_date); ?></td>
                                    </tr>
                                    <tr>
                                        <th>น้ำหนัก/ถุง (g.)</th><td><?php echo e(number_format($packaging->inner_weight_g,2,".",",")); ?></td>
                                    </tr>
                                    <tr>
                                        <th>จำนวนถุง/กล่อง</th><td><?php echo e($packaging->number_per_pack); ?></td>
                                    </tr>
                                    <tr>
                                        <th>น้ำหนัก/กล่อง (kg.)</th><td><?php echo e(number_format($packaging->outer_weight_kg,3,".",",")); ?></td>
                                    </tr>
                                     <tr>
                                        <th>Package</th><td>
                                            <?php $__currentLoopData = $packaging->package()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php echo e($subitem->packagetype->name); ?> \ 
                                                    <a href="<?php echo e(url('packagings/showfile/'. $subitem->id)); ?>" target="_blank" ><?php echo e($subitem->name); ?></a> \ <?php echo e($subitem->size); ?>     
                                                     <?php if(isset($packageexp[$subitem->id])): ?>
                                                      \ วิธีการ Stamp : '<?php echo e($packageexp[$subitem->id]); ?>'
                                                    <?php endif; ?>                                          
                                               <br/>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>เครื่อง Stamp</th><td>
                                            <?php $__currentLoopData = $packaging->stamp()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php echo e($subitem->name); ?>                                            
                                               <br/>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>เครื่อง Pack</th><td>
                                            <?php $__currentLoopData = $packaging->packmachine()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php echo e($subitem->name); ?>                                           
                                               <br/>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
            </div>
        </div>
    </div>
 <?php if (isset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da)): ?>
<?php $component = $__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da; ?>
<?php unset($__componentOriginal8e2ce59650f81721f93fef32250174d77c3531da); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH D:\Projects\IRD\PackageSystem\PackageSystem\resources\views/packagings/show.blade.php ENDPATH**/ ?>