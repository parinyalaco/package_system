<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Product Group {{ $productgroup->id }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{ url('/product-groups') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/product-groups/' . $productgroup->id . '/edit') }}" title="Edit ProductGroup"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @if ($productgroup->products->count() == 0)
                        <form method="POST" action="{{ url('productgroups' . '/' . $productgroup->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete ProductGroup" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $productgroup->id }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Name</th><td>{{ $productgroup->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $productgroup->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $productgroup->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                <h4>Products </h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Product name</th>
                                 <th>Customer</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->customer->name ?? '' }}</td>
                                    <td><a href="{{ url('/products/' . $item->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
</td>
                                </tr>    
                            @endforeach
                            
                        </tbody>
                    </table>   
                    <div class="pagination-wrapper"> {!! $products->render() !!} </div>
                       
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
