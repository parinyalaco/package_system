<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            PackMachine {{ $packmachine->id }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <a href="{{ url('/pack-machines') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/pack-machines/' . $packmachine->id . '/edit') }}" title="Edit PackMachine"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @if ($packmachine->packagings->count() == 0)
                        <form method="POST" action="{{ url('packmachines' . '/' . $packmachine->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete PackMachine" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>    
                        @endif
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $packmachine->id }}</td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th>Name</th><td>{{ $packmachine->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $packmachine->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $packmachine->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <h4>Packaging</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Packaging version</th>
                                <th>Product</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packagings as $item)
                                <tr>
                                    <td>
                                        {{ $item->version }}</td>
                                        <td>
                                        {{ $item->product->name ?? ''}}</td>
                                        <td><a href="{{ url('/packagings/' . $item->id . '/edit') }}" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a></td>
                                </tr>    
                            @endforeach
                            
                        </tbody>
                    </table>
                     <div class="pagination-wrapper"> {!! $packagings->render() !!} </div>
            </div>
            </div>
        </div>
    </div>
</x-app-layout>
