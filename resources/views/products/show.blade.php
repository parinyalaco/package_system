<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Product {{ $product->id }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{ url('/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/products/' . $product->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('products' . '/' . $product->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $product->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Customer</th><td>{{ $product->customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Product Group</th><td>{{ $product->productgroup->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th><td>{{ $product->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>SAP</th><td>{{ $product->SAP }}</td>
                                    </tr>
                                    <tr>
                                        <th>Shelf Lift (เดือน)</th><td>{{ $product->shelf_life }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $product->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $product->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                         <h4>Packaging</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Packaging version</th>
                                <th>Product</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packagings as $item)
                                <tr>
                                    <td>
                                        {{ $item->version }}</td>
                                        <td>
                                        {{ $item->product->name ?? ''}}</td>
                                    <td><a href="{{ url('/packagings/' . $item->id . '/edit') }}" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
</td>
                                </tr>    
                            @endforeach
                            
                        </tbody>
                    </table>
                     <div class="pagination-wrapper"> {!! $packagings->render() !!} </div>
            </div>
            </div>
        </div>
    </div>
</x-app-layout>


