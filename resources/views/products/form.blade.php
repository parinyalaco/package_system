
<div class="row">
<div class="col-md-4 {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', 'Customer', ['class' => 'control-label']) !!}
        @if (isset($product->customer_id))
            {!! Form::select('customer_id', $customerlist,$product->customer_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('customer_id', $customerlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('product_group_id') ? 'has-error' : ''}}">
        {!! Form::label('product_group_id', 'Product Group', ['class' => 'control-label']) !!}
        @if (isset($product->customer_id))
            {!! Form::select('product_group_id', $productgrouplist,$product->product_group_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('product_group_id', $productgrouplist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('product_group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $product->name ?? ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('shelf_life') ? 'has-error' : ''}}">
    <label for="shelf_life" class="control-label">{{ 'Shelf life (เดือน)' }}</label>
    <input class="form-control" name="shelf_life" type="number" id="shelf_life" required value="{{ $product->shelf_life ?? '0'}}" >
    {!! $errors->first('shelf_life', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'status' }}</label>
    <input class="form-control" name="status" type="text" id="status" required value="{{ $product->status ?? 'Active'}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('SAP') ? 'has-error' : ''}}">
    <label for="SAP" class="control-label">{{ 'SAP' }}</label>
    <input class="form-control" name="SAP" type="text" id="SAP" required value="{{ $product->SAP ?? ''}}" >
    {!! $errors->first('SAP', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $product->desc ?? ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4 form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>