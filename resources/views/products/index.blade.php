<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Products
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{ url('/products/create') }}" class="btn btn-success btn-sm" title="Add New Product">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="{{ url('/products') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer<br/>
                                            <input type="text" class="form-control" name="txtcustomer" placeholder="ค้นลูกค้า..." value="{{ request('txtcustomer') }}">
                                        </th>
                                        <th>
                                            Product Group<br/>
                                            <input type="text" class="form-control" name="txtproductgroup" placeholder="ค้นกลุ่มสินค้า..." value="{{ request('txtproductgroup') }}">
                                        </th>
                                        <th>Product Name<br/>
                                            <input type="text" class="form-control" name="txtproduct" placeholder="ค้นสินค้า..." value="{{ request('txtproduct') }}">
                                        </th>
                                        <th>Used</th>
                                        <th>Actions<br/><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                @foreach($products as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->customer->name ?? ''}}</td>
                                        <td>{{ $item->productgroup->name ?? ''}}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->packagings()->count() ?? '0'}}</td>
                                        
                                        <td>
                                            <a href="{{ url('/products/' . $item->id) }}" title="View Product"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/products/' . $item->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            @if ($item->packagings()->count() == 0)
                                            <form method="POST" action="{{ url('/products' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>    
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            
                            <div class="pagination-wrapper"> {!! $products->appends(
                                [
                                    'txtcustomer' => Request::get('txtcustomer'),
                                    'txtproductgroup' => Request::get('txtproductgroup'),
                                    'txtproduct' => Request::get('txtproduct')
                                ])->render() !!} </div>
                        </div>
            </div>
        </div>
    </div>
</x-app-layout>
