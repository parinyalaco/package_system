<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Packagings
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        @if (Auth::user()->grouptype == 'Admin')                                                    
                            <a href="{{ url('/packagings/createwithadd') }}" class="btn btn-success btn-sm" title="Add New Packaging">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        @endif
                            <div class="input-group">
                                <span class="input-group-append">
                                </span>
                            </div>
                        

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="{{ url('/packagings') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">                        
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Version<br/><input type="text" class="form-control" name="txtversion" placeholder="ค้นหา Verion..." value="{{ request('txtversion') }}"></th>
                                        <th>Product Group<br/><input type="text" class="form-control" name="txtproductgroup" placeholder="ค้นหา กลุ่มสินค้า..." value="{{ request('txtproductgroup') }}"></th>                                                 
                                        <th>Product<br/><input type="text" class="form-control" name="txtproduct" placeholder="ค้นหา สินค้า..." value="{{ request('txtproduct') }}"></th>         
                                        <th>Package<br/><input type="text" class="form-control" name="txtpackage" placeholder="ค้นหา Package..." value="{{ request('txtpackage') }}"></th>                                    
                                        <th>น้ำหนักต่อถุง(g.) <br/>/ จำนวนถุงต่อกล่อง<br/> / น้ำหนักต่อกล่อง(kg.)<br/><input type="text" class="form-control" name="txtweight" placeholder="ค้นหา นน จำนวน..." value="{{ request('txtweight') }}"></th>
                                        <th>Actions<br/>
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                @foreach($packagings as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->version }}</td>                                        
                                        <td>{{ $item->product->productgroup->name ?? ''}}</td>
                                        <td>{{ $item->product->name }}</td>
                                        
                                        <td>
                                            @foreach ($item->package()->get() as $subitem)
                                               {{ $subitem->packagetype->name }} \ {{ $subitem->name }} <br/>
                                            @endforeach
                                        </td>
                                        <td>{{ number_format($item->inner_weight_g,2,".",",") }} / {{ $item->number_per_pack }} / {{ number_format($item->outer_weight_kg,3,".",",") }}</td>
                                        <td>
                                            <a href="{{ url('/packagings/' . $item->id) }}" title="View Packaging"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/packagings/exportdoc/' . $item->id) }}" title="QP07 Packaging"><button class="btn btn-info btn-sm"><i class="fa fa-file-text-o" aria-hidden="true"></i> QP07</button></a>
                                 
                                            @if (Auth::user()->grouptype == 'Admin') 
                                            <a href="{{ url('/packagings/clone/' . $item->id ) }}" title="Edit Packaging"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> clone</button></a>

                                            <a href="{{ url('/packagings/editwithadd/' . $item->id ) }}" title="Edit Packaging"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/packagings' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Packaging" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $packagings->appends([
                                'txtversion' => Request::get('txtversion'),
                                'txtproductgroup' => Request::get('txtproductgroup'),
                                'txtproduct' => Request::get('txtproduct'),
                                'txtpackage' => Request::get('txtpackage'),
                                'txtweight' => Request::get('txtweight')
                                ])->render() !!} </div>
                        </div>

            </div>
        </div>
    </div>
</x-app-layout>