<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            PackageType {{ $packagetype->id }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{ url('/package-types') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/package-types/' . $packagetype->id . '/edit') }}" title="Edit PackageType"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @if ($packagetype->packages->count() == 0)     
                        <form method="POST" action="{{ url('packagetypes' . '/' . $packagetype->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete PackageType" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $packagetype->id }}</td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th>Name</th><td>{{ $packagetype->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $packagetype->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th><td>{{ $packagetype->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                <h4>Package </h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Package name</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packages as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td><a href="{{ url('/packages/' . $item->id . '/edit') }}" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
</td>
                                </tr>    
                            @endforeach
                            
                        </tbody>
                    </table>
                     <div class="pagination-wrapper"> {!! $packages->render() !!} </div>
                        
                </div>


            </div>
        </div>
    </div>
</x-app-layout>
