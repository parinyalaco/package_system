<div class="row">
<div class="col-md-4 {{ $errors->has('package_type_id') ? 'has-error' : ''}}">
        {!! Form::label('package_type_id', 'Package Type', ['class' => 'control-label']) !!}
        @if (isset($package->package_type_id))
            {!! Form::select('package_type_id', $packagetypelist,$package->package_type_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('package_type_id', $packagetypelist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('package_type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $package->name ?? ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'status' }}</label>
    <input class="form-control" name="status" type="text" id="status" required value="{{ $package->status ?? 'Active'}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $package->desc ?? ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('sapnote') ? 'has-error' : ''}}">
    <label for="sapnote" class="control-label">{{ 'SAP data' }}</label>
    <textarea class="form-control" name="sapnote" type="text" id="sapnote">{{ $package->sapnote ?? ''}}</textarea>
    {!! $errors->first('sapnote', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('note2') ? 'has-error' : ''}}">
    <label for="note2" class="control-label">{{ 'รายละเอียด เพิ่มเติม' }}</label>
    <textarea class="form-control" name="note2" type="text" id="note2">{{ $package->note2 ?? ''}}</textarea>
    {!! $errors->first('note2', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-6  {{ $errors->has('stamp_format') ? 'has-error' : ''}}">
    <label for="stamp_format" class="control-label">{{ 'Stamp Format' }}</label>
    <input class="form-control" name="stamp_format" type="text" id="stamp_format" required value="{{ $package->stamp_format ?? ''}}" >
    {!! $errors->first('stamp_format', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-6  {{ $errors->has('size') ? 'has-error' : ''}}">
    <label for="size" class="control-label">{{ 'Size' }}</label>
    <input class="form-control" name="size" type="text" id="size" required value="{{ $package->size ?? ''}}" >
    {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-6  {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image PATH' }}</label>
    <input class="form-control" name="image" type="text" id="image" required value="{{ $package->image ?? ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('relate_id') ? 'has-error' : ''}}">
        {!! Form::label('relate_id', 'Package เก่า (ถ้ามี)', ['class' => 'control-label']) !!}
        @if (isset($package->relate_id))
            {!! Form::select('relate_id', $packagelist,$package->relate_id, ['class' => 'form-control caldate getorderlist getprice','placeholder'=>'เลือก']) !!}   
        @else
            {!! Form::select('relate_id', $packagelist,null, ['class' => 'form-control caldate getorderlist  getprice','placeholder'=>'เลือก']) !!}
        @endif
        {!! $errors->first('relate_id', '<p class="help-block">:message</p>') !!}
</div>
<div class=" col-md-4">
    <input class="form-group btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>