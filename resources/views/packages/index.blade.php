<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Packages
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <a href="{{ url('/packages/create') }}" class="btn btn-success btn-sm" title="Add New Package">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        
                            
                        

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <form method="GET" action="{{ url('/packages') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type<br/><input type="text" class="form-control" name="txttype" placeholder="ค้นหาประเภท..." value="{{ request('txttype') }}"></th>
                                        <th>Name<br/><input type="text" class="form-control" name="txtname" placeholder="ค้นหาชื่อ..." value="{{ request('txtname') }}"></th>
                                        <th>Size<br/><input type="text" class="form-control" name="txtsize" placeholder="ค้นหาขนาด..." value="{{ request('txtsize') }}"></th>
                                        <th>Used</th>
                                        <th>Status<br/>{!! Form::select('txtstatus', array('Active'=>'Active','Inactive'=>'Inactive'),request('txtstatus')) !!}</th>                                        
                                        
                                        <th>Actions<br><button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button></th>
                                    </tr>
                                </thead>
                                </form>
                                <tbody>
                                @foreach($packages as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->packagetype->name ?? '' }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->size }}</td>
                                        <td>{{ $item->packagings()->count() ?? '0' }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <a href="{{ url('/packages/' . $item->id) }}" title="View Package"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/packages/' . $item->id . '/edit') }}" title="Edit Package"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            @if ($item->packagings()->count() == 0)
                                            <form method="POST" action="{{ url('/packages' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Package" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>    
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $packages->appends([
                                'txttype' => Request::get('txttype'),
                                'txtname' => Request::get('txtname'),
                                'txtsize' => Request::get('txtsize'),
                                'txtstatus' => Request::get('txtstatus'),
                                ])->render() !!} </div>
                        </div>
            </div>
        </div>
    </div>
</x-app-layout>
