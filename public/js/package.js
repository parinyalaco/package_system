$(document).ready(function () {

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


    $('.calweight').change(function () {
        var innerweight = parseFloat($('#inner_weight_g').val());
        var numperpack = parseInt($('#number_per_pack').val());
        var totalweight = (innerweight * numperpack / 1000);
        $('#outer_weight_kg').val(totalweight);
    });

    $(".packagedata").click(function () {
        $("#dd-" + $(this).val()).toggle("slow");
    });


    $('#btn-add-new-package-type').click(function(){
        
        $.get('/packaging/packages/getpackagelist/?' + $('#frmaddnewpackagetype').serialize()).done(function (data) {
            var listItems = "";
            //add div with dropdown    
            listItems += '<label for="product_id" class="control-label">' + data.maindata.name +'</label>' ;
            listItems += '<select class="form-control" name="new-package-type[]">';
            listItems += "<option value=''>===Select===</option>";
            $.each(data.listdata, function (key, entry) {
                listItems += "<option value='" + entry + "'>" + key + "</option>";
            })
            listItems += '</select>';
            listItems += '<br/>';
          listItems += '<input class="form-control" name="exp-type[]" type="text" value="" placeholder="ใส่วิธีการ Stamp ' + data.maindata.name +'">';
            $('#morepackagetype').append(listItems);
        }).fail(function () {
            alert("error");
        });



        modal.style.display = "none";
    });

});