<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\PackageType;
use Illuminate\Http\Request;

class PackageTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $packagetypes = PackageType::where('name','like','%'.$keyword.'%')->latest()->paginate($perPage);
        } else {
            $packagetypes = PackageType::latest()->paginate($perPage);
        }

        return view('package-types.index', compact('packagetypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('package-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        PackageType::create($requestData);

        return redirect('package-types')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $perPage = 20;
        $packagetype = PackageType::findOrFail($id);

        $packages = $packagetype->packages()->latest()->paginate($perPage);

        return view('package-types.show', compact('packagetype', 'packages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $packagetype = PackageType::findOrFail($id);

        return view('package-types.edit', compact('packagetype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $packagetype = PackageType::findOrFail($id);
        $packagetype->update($requestData);

        return redirect('package-types')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PackageType::destroy($id);

        return redirect('package-types')->with('flash_message', ' deleted!');
    }
}
