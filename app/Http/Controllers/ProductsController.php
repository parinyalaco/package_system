<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProductGroup;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $txtcustomer = $request->get('txtcustomer');
        $txtproductgroup = $request->get('txtproductgroup');
        $txtproduct = $request->get('txtproduct');
        $perPage = 25;

        $productObj = new Product();

        if(!empty($txtcustomer)){
            $customerList = Customer::where('name','like','%'.$txtcustomer.'%')->pluck('id');
            $productObj = $productObj->whereIn('customer_id', $customerList);
        }

        if (!empty($txtproductgroup)) {
            $productGroupList = ProductGroup::where('name', 'like', '%' . $txtproductgroup . '%')->pluck('id');
            $productObj = $productObj->whereIn('product_group_id', $productGroupList);
        }

        if (!empty($txtproduct)) {
            $productObj = $productObj->where('name','like','%'. $txtproduct.'%');
        }

        $products = $productObj->paginate($perPage);

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $customerlist = Customer::pluck('name', 'id');
        $productgrouplist = ProductGroup::pluck('name', 'id');

        return view('products.create',compact('customerlist', 'productgrouplist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        Product::create($requestData);

        return redirect('products')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $perPage = 20;
        $product = Product::findOrFail($id);
        $packagings = $product->packagings()->latest()->paginate($perPage);

        return view('products.show', compact('product', 'packagings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $customerlist = Customer::pluck('name', 'id');
        $productgrouplist = ProductGroup::pluck('name', 'id');

        return view('products.edit', compact('product', 'customerlist', 'productgrouplist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $product = Product::findOrFail($id);
        $product->update($requestData);

        return redirect('products')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect('products')->with('flash_message', ' deleted!');
    }
}
