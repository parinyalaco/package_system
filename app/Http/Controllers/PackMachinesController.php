<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\PackMachine;
use Illuminate\Http\Request;

class PackMachinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $packmachines = PackMachine::latest()->paginate($perPage);
        } else {
            $packmachines = PackMachine::latest()->paginate($perPage);
        }

        return view('pack-machines.index', compact('packmachines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('pack-machines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        PackMachine::create($requestData);

        return redirect('pack-machines')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $perPage = 20;
        $packmachine = PackMachine::findOrFail($id);

        $packagings = $packmachine->packagings()->orderBy('packagings.created_at')->paginate($perPage);

        return view('pack-machines.show', compact('packmachine', 'packagings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $packmachine = PackMachine::findOrFail($id);

        return view('pack-machines.edit', compact('packmachine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $packmachine = PackMachine::findOrFail($id);
        $packmachine->update($requestData);

        return redirect('pack-machines')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PackMachine::destroy($id);

        return redirect('pack-machines')->with('flash_message', ' deleted!');
    }
}
