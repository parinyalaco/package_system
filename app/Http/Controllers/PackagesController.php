<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Package;
use App\Models\PackageType;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $txttype = $request->get('txttype');
        $txtname = $request->get('txtname');
        $txtsize = $request->get('txtsize');
        $txtstatus = $request->get('txtstatus');
        $perPage = 25;

        $packageObj = new Package();

        if (!empty($txttype)) {
            $typelist = PackageType::where('name','like','%'. $txttype.'%')->pluck('id');

            $packageObj = $packageObj->whereIn('package_type_id',$typelist);
        }

        if (!empty($txtname)) {
            $packageObj = $packageObj->where('name', 'like', '%' . $txtname . '%');
        }

        if (!empty($txtsize)) {
            $packageObj = $packageObj->where('size', 'like', '%' . $txtsize . '%');
        }

        if (!empty($txtstatus)) {
            $packageObj = $packageObj->where('status', '=', $txtstatus );
        } 

        $packages = $packageObj->latest()->paginate($perPage);

        return view('packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $packagetypelist = PackageType::pluck('name', 'id');
        $packagelist = Package::pluck('name', 'id');
        
        return view('packages.create',compact('packagetypelist', 'packagelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        Package::create($requestData);

        return redirect('packages')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $perPage = 20;
        $package = Package::findOrFail($id);
        $packagings = $package->packagings()->latest()->paginate($perPage);
        $packagetypelist = PackageType::pluck('name', 'id');

        return view('packages.show', compact('package', 'packagetypelist', 'packagings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        $packagetypelist = PackageType::pluck('name', 'id');
        $packagelist = Package::pluck('name', 'id');

        return view('packages.edit', compact('package', 'packagetypelist', 'packagelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $package = Package::findOrFail($id);
        $package->update($requestData);

        return redirect('packages')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Package::destroy($id);

        return redirect('packages')->with('flash_message', ' deleted!');
    }

    public function getpackagelist(Request $request){
        $requestData = $request->all();
        //var_dump($requestData);
        $packagetype = PackageType::findOrFail($requestData['add-new-package-type']);
        $packagetypelist = Package::Where('package_type_id', $requestData['add-new-package-type'])->orderBy('name','asc')->pluck( 'id', 'name');
        return response()->json(array('listdata' => $packagetypelist, 'maindata' => $packagetype));
    }
}
