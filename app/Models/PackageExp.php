<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageExp extends Model
{
    use HasFactory;
    protected $fillable = ['packaging_id', 'package_id', 'stamp_type'];

    
}
